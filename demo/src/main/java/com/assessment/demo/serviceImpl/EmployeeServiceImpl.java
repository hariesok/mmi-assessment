package com.assessment.demo.serviceImpl;

import java.util.List;
import java.util.Optional;

import com.assessment.demo.models.EmployeeModel;
import com.assessment.demo.repositories.EmployeeRepository;
import com.assessment.demo.services.EmployeeService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class EmployeeServiceImpl implements EmployeeService {
    @Autowired
    private EmployeeRepository employeerepo;

    @Override
    public List<EmployeeModel> getAllEmployee() {
        return employeerepo.getAllEmployee();
    }

    @Override
    public void delete(int id) {
        employeerepo.deleteById(id);
    }

    @Override
    public EmployeeModel save(EmployeeModel Employee) {
        return employeerepo.save(Employee);
    }

    @Override
    public Optional<EmployeeModel> findById(int id) {
        return employeerepo.findById(id);
    }

    @Override
    public List<EmployeeModel> findEmployee(String name, int idNumber) {
        return employeerepo.findEmployee(name, idNumber);
    }
    
}