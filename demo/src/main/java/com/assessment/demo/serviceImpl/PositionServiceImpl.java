package com.assessment.demo.serviceImpl;

import java.util.List;

import com.assessment.demo.models.PositionModel;
import com.assessment.demo.repositories.PositionRepository;
import com.assessment.demo.services.PositionService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PositionServiceImpl implements PositionService {
    @Autowired
    private PositionRepository positionrepo;

    @Override
    public List<PositionModel> getAllPosition() {
        return positionrepo.getAllPosition();
    }
    
}