package com.assessment.demo.services;

import java.util.List;

import com.assessment.demo.models.PositionModel;

public interface PositionService {
    List<PositionModel> getAllPosition();
}