package com.assessment.demo.restControllers;

import com.assessment.demo.models.EmployeeModel;
import com.assessment.demo.services.EmployeeService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;


@RestController
@RequestMapping(path="/api/employee", produces = "application/json")
@CrossOrigin(origins = "*")
public class EmployeeRestController {
    @Autowired
    private EmployeeService employees;

    @GetMapping("/")
    public ResponseEntity<?> getAllEmployee() {
        return new ResponseEntity<>(employees.getAllEmployee(), HttpStatus.OK);
    }

    @PostMapping("/")
    public ResponseEntity<?> saveEmployee(@RequestBody EmployeeModel souvenir){
        return new ResponseEntity<>(employees.save(souvenir), HttpStatus.OK);
    }
    @GetMapping("/{id}")
    public ResponseEntity<?> getEmployeeDetail(@PathVariable("id") int id) {
        return new ResponseEntity<>(employees.findById(id), HttpStatus.OK);
    }
    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteEmployee(@PathVariable("id") int id) {
        try {
            employees.delete(id);
        } catch (Exception e) {
            System.out.println(e);
        }
    }
    
}