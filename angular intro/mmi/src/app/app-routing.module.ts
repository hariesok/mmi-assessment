import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {AddKaryawanComponent} from './add-karyawan/add-karyawan.component';
import {ListKaryawanComponent} from './list-karyawan/list-karyawan.component'
import { DetailsKaryawanComponent } from './details-karyawan/details-karyawan.component';

const routes: Routes = [
  {path: 'karyawanEditAdd', component: AddKaryawanComponent},
  {path: 'karyawanindex', component: ListKaryawanComponent},
  {path: '', redirectTo: '/karyawanindex', pathMatch: 'full'},
  {path: 'details/:id', component: DetailsKaryawanComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
