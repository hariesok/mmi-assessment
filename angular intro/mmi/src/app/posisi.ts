export interface Posisi {
    id: number;
    code: string;
    name: string;
}