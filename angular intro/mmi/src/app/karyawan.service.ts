import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class KaryawanService {
  private baseUrl = 'http://localhost:8088/api/employee/';
  constructor(private http: HttpClient) { }

  getKaryawan(id: number): Observable<any> {
    return this.http.get(this.baseUrl + id);
  }
  createKaryawan(employee: Object): Observable<Object>{
    return this.http.post(this.baseUrl, employee);
  }
  updateKaryawan(id: number, value: any): Observable<Object> {
    return this.http.put(this.baseUrl + id, value);
  }
  deleteKaryawan(id: number): Observable<any> {
    return this.http.delete(this.baseUrl + id, {responseType: 'text'});
  }

  getListKaryawan(): Observable<any> {
    return this.http.get(this.baseUrl);
  }
  getPositionList(): Observable<any> {
    return this.http.get('http://localhost:8088/api/position/')
  }
}