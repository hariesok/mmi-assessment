import { Component, OnInit } from '@angular/core';
import {DetailsKaryawanComponent} from '../details-karyawan/details-karyawan.component';
import {Observable} from 'rxjs';
import {KaryawanService} from '../karyawan.service';
import {Karyawan} from '../karyawan';
import {Router} from '@angular/router';

@Component({
  selector: 'app-list-karyawan',
  templateUrl: './list-karyawan.component.html',
  styleUrls: ['./list-karyawan.component.css']
})
export class ListKaryawanComponent implements OnInit {
  karyawan: Observable<Karyawan[]>;
  constructor(private karyawanService: KaryawanService, private router: Router) { }

  ngOnInit(): void {
    this.reloadData();
  }
  reloadData() {
    this.karyawan = this.karyawanService.getListKaryawan();
  }
  deleteKaryawan(id: number) {
    this.karyawanService.deleteKaryawan(id)
    .subscribe(
      data => {
        console.log(data);
        this.reloadData();
      }, error => console.log(error)
    );
  }
  editKaryawan(id: number) {
    this.router.navigate(['details', id])
  }
  addKaryawan() {
    this.router.navigate(['karyawanEditAdd'])
  }

}
