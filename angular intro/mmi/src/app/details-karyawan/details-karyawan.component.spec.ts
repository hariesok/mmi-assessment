import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DetailsKaryawanComponent } from './details-karyawan.component';

describe('DetailsKaryawanComponent', () => {
  let component: DetailsKaryawanComponent;
  let fixture: ComponentFixture<DetailsKaryawanComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DetailsKaryawanComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DetailsKaryawanComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
