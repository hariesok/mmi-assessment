import { Component, OnInit } from '@angular/core';
import {Karyawan} from '../karyawan';
import {Posisi} from '../posisi';
import {KaryawanService} from '../karyawan.service'
import {Router} from '@angular/router';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-add-karyawan',
  templateUrl: './add-karyawan.component.html',
  styleUrls: ['./add-karyawan.component.css']
})
export class AddKaryawanComponent implements OnInit {

  karyawan: Karyawan;
  posisi: Observable<Posisi[]>;
  constructor(private karyawanService: KaryawanService, private router: Router) { }

  ngOnInit(): void {
    this.loadData();
  }

  loadData() {
    this.posisi = this.karyawanService.getPositionList();
  }

  saveKaryawan() {
    alert("coming soon.");
  }

  backToList() {
    this.router.navigate(['karyawanindex']);
  }
}
