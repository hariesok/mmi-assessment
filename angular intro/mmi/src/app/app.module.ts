import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {FormsModule} from '@angular/forms'
import { AppRoutingModule } from './app-routing.module';
import {HttpClientModule} from '@angular/common/http';

import { AppComponent } from './app.component';
import { ListKaryawanComponent } from './list-karyawan/list-karyawan.component';
import { AddKaryawanComponent } from './add-karyawan/add-karyawan.component';
import { DetailsKaryawanComponent } from './details-karyawan/details-karyawan.component';

@NgModule({
  declarations: [
    AppComponent,
    ListKaryawanComponent,
    AddKaryawanComponent,
    DetailsKaryawanComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    AppRoutingModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
