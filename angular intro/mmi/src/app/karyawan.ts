export interface Karyawan {
    id: number;
    name: string;
    birthDate: Date;
    positionId: number;
    position: {
        code: string;
        name: string
    }
    idNumber: number;
    gender: number;
    
}